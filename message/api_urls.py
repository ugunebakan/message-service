from rest_framework import routers
from message.viewsets import MessageViewSet


app_name = 'message-app'

router = routers.SimpleRouter()
router.register('', MessageViewSet, basename='message')

urlpatterns = router.urls
