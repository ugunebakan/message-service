from django.db import models
from user.models import User


class Message(models.Model):
    message = models.TextField()
    from_user = models.ForeignKey(
        User,
        related_name='sent_messages',
        on_delete=models.CASCADE,
    )
    to_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='received_messages',
    )
    is_read = models.BooleanField(
        default=False,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
