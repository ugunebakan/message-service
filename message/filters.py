import django_filters
from django_filters.rest_framework import FilterSet
from django_filters import rest_framework as filters

from message.models import Message


class MessageFilter(FilterSet):
    created_at = django_filters.CharFilter('created_at')
    start_time = django_filters.DateTimeFilter(field_name='created_at', lookup_expr='gte')
    end_time = django_filters.DateTimeFilter(field_name='created_at', lookup_expr='lte')

    ordering = django_filters.OrderingFilter(
        fields=(
            ('created_at', 'created_at'),
        )
    )

    class Meta:
        model = Message
        fields = [
            'created_at',
            'start_time',
            'end_time',
        ]
