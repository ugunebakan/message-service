from django.db.models import Q
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    mixins,
    status,
    viewsets,
)
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView

from message.filters import MessageFilter
from message.models import Message
from message.serializers import (
    MessageSerializer,
    BulkMessageSerializer,
    MessageUserSerializer,
)


class MessageViewSet(viewsets.ModelViewSet):
    """
    Message API.
    You can submit a message (1) with POST request to a user.
    You can DELETE a single(3) message or delete multiple(4) messages
    with POST request (sending payload to HTTP DELETE request is not
    reccomended).
    You can fetch all messages ordered by time with query parameter (5):
        '/?ordering=created_at' or '/?ordering=-created_at'
    You can set start and end date to fetch messages for a given range:
        '/?start_time=2021-11-02%2017:12:04.762474%20&end_time=2021-11-02%2019:41:24.934916'
    """
    queryset = Message.objects.prefetch_related(
        'from_user',
        'to_user',
    )
    serializer_class = MessageSerializer
    filter_class = MessageFilter
    filter_backends = (DjangoFilterBackend,)
    http_method_names = ['get', 'post', 'delete', 'option']

    @action(methods=['post'],
            detail=False,
            serializer_class=BulkMessageSerializer
            )
    def bulkdelete(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        Message.objects.filter(id__in=serializer.data['ids']).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserMessageViewSet(
    viewsets.ViewSet,
    GenericAPIView,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()

    def destroy(self, request, pk=None, user_pk=None):
        instance = get_object_or_404(Message, pk=pk, from_user=user_pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def create(self, request, user_pk=None):
        request.data['from_user'] = user_pk
        return super().create(request, user_pk)

    def list(self, request, user_pk=None):
        filter_params = Q(from_user=user_pk) | Q(to_user=user_pk)
        queryset = Message.objects.filter(filter_params)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, user_pk=None):
        filter_params = Q(from_user=user_pk) | Q(to_user=user_pk)
        queryset = Message.objects.filter(filter_params)
        instance = get_object_or_404(queryset, pk=pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
