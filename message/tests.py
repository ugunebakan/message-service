from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse

from message.models import Message
from user.models import User


class MessageTests(APITestCase):

    def setUp(self):
        self.users_data = [
            {
                'username': 'umut.gunebakan',
                'email': 'u@z.com',
                'first_name': 'umut',
                'last_name': 'gunebakan',
            },
            {
                'username': 'adem.gunebakan',
                'email': 'a@z.com',
                'first_name': 'adem',
                'last_name': 'gunebakan',
            },
            {
                'username': 'test.gunebakan',
                'email': 't@z.com',
                'first_name': 'test',
                'last_name': 'gunebakan',
            }
        ]

        self.messages_data = [
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        ]
        users = [User(**user) for user in self.users_data]
        User.objects.bulk_create(users)
        self.user_ids = list(User.objects.all().values_list('id', flat=True))

        for data in self.messages_data:
            message_body = {
                'message': data,
                'from_user_id': self.user_ids[0],
                'to_user_id': self.user_ids[1]
            }
            Message.objects.create(**message_body)

    def test_get_messages(self):
        url = reverse('message-app:message-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), Message.objects.count())

    def test_create_message(self):
        url = reverse('message-app:message-list')
        message_payload = {
            'message': 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur',
            'from_user': self.user_ids[1],
            'to_user': self.user_ids[0]
        }
        response = self.client.post(url, message_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json().get('message'), message_payload.get('message'))

    def test_get_single_message(self):
        for _id in Message.objects.all().values_list('id', flat=True):
            url = reverse('message-app:message-detail', args=[_id])
            response = self.client.get(url, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTrue(response.json().get('message') in self.messages_data)

    def test_bulk_delete_message(self):
        url = reverse('message-app:message-bulkdelete')
        ids = list(Message.objects.all().values_list('id', flat=True))[:-1]
        payload = {
            'ids': ids,
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        for _id in ids:
            self.assertFalse(Message.objects.filter(id=_id).exists())

    def test_delete_single_message(self):
        _id = Message.objects.first().id
        url = reverse('message-app:message-detail', args=[_id])

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Message.objects.filter(id=_id).exists())

    def test_fetch_new_messages(self):
        user_id = self.user_ids[0]
        url = reverse('user-app:user-fetchnewmessages', args=[user_id])

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json().get('count') > 0)

        # new messages are fetched, no more new messages
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json().get('count') == 0)

    def test_get_user_messages(self):
        user_id = self.user_ids[0]
        url = reverse('user-app:message-list', args=[user_id])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json().get('count') > 0)

    def test_send_message_to_a_user(self):
        from_user = self.user_ids[0]
        to_user = self.user_ids[1]
        url = reverse('user-app:message-list', args=[from_user])

        message_text = 'this is an awesome message'

        payload = {
            'message': message_text,
            'to_user': to_user,
        }

        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Message.objects.filter(message=message_text).exists())

    def test_get_single_from_user_message(self):
        message_id, user_id = Message.objects.all().values_list('id', 'from_user_id')[0]
        url = reverse('user-app:message-detail', args=[user_id, message_id])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_to_user_message(self):
        message_id, user_id = Message.objects.all().values_list('id', 'to_user_id')[0]
        url = reverse('user-app:message-detail', args=[user_id, message_id])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def user_cannot_see_others_messages(self):
        message_id, from_user_id, to_user_id = Message.objects.all().values_list('id', 'from_user_id', 'to_user_id')[0]
        other_user_id = list(set(self.user_ids) - set([from_user_id, to_user_id]))
        url = reverse('user-app:message-detail', args=[other_user_id, message_id])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_delete_single_to_user_message(self):
        # user can only delete the messages that user sent
        message_id, user_id = Message.objects.all().values_list('id', 'to_user_id')[0]
        url = reverse('user-app:message-detail', args=[user_id, message_id])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(Message.objects.filter(id=message_id).exists())

    def test_cannot_delete_single_to_user_message(self):
        # user can only delete the messages that user sent
        message_id, user_id = Message.objects.all().values_list('id', 'from_user_id')[0]
        url = reverse('user-app:message-detail', args=[user_id, message_id])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Message.objects.filter(id=message_id).exists())
