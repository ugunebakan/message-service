from rest_framework import serializers
from message.models import Message


class BulkMessageSerializer(serializers.Serializer):
    ids = serializers.ListField(
        child=serializers.SlugRelatedField(
            queryset=Message.objects.all(),
            slug_field='id',
        )
    )


class MessageSerializer(serializers.ModelSerializer):
    created_at = serializers.ReadOnlyField()
    updated_at = serializers.ReadOnlyField()
    is_read = serializers.ReadOnlyField()
    from_username = serializers.ReadOnlyField(source='from_user.username')
    to_username = serializers.ReadOnlyField(source='to_user.username')

    class Meta:
        model = Message
        fields = [
            'id',
            'message',
            'from_username',
            'to_username',
            'from_user',
            'to_user',
            'is_read',
            'created_at',
            'updated_at',
        ]


class MessageUserSerializer(serializers.ModelSerializer):
    created_at = serializers.ReadOnlyField()
    updated_at = serializers.ReadOnlyField()
    is_read = serializers.ReadOnlyField()
    from_username = serializers.ReadOnlyField(source='from_user.username')
    to_username = serializers.ReadOnlyField(source='to_user.username')

    class Meta:
        model = Message
        fields = [
            'id',
            'message',
            'from_username',
            'to_username',
            'to_user',
            'is_read',
            'created_at',
            'updated_at',
        ]
