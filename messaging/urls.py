import debug_toolbar
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Messaging Service')


urlpatterns = [
    url(r'docs/', schema_view, name='docs'),
    url(r'user/', include('user.api_urls')),
    url(r'message/', include('message.api_urls')),
    path('admin/', admin.site.urls),
    path('__debug__/', include(debug_toolbar.urls)),
]
