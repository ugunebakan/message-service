## Local Setup

Create a virtual environment to install dependencies in and activate it:

```sh
$ git clone https://bitbucket.org/ugunebakan/message-service.git
$ cd message-service
$ virtualenv env -p python3
$ source env/bin/activate

Windows:
$ py -3 -m venv env
$ env\scripts\activate
$ pip install -r requirements.txt
```

Then install the dependencies:

```sh
(env)$ pip install -r requirements.txt
```
Note the `(env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv2`.

Once `pip` has finished downloading the dependencies:

```sh
(env)$ python manage.py migrate
(env)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/`, or navigate to `http://127.0.0.1:8000/docs`.

## Docker Setup
To create the docker image and run it
```sh
$ docker build . -t message-serive:latest
$ docker run -p 8000:8000 --name umut message-serive:latest
```

While docker container is up, to run the migrations use the command below on another termial window
```sh
$ docker exec umut python manage.py migrate
```
And navigate to `http://127.0.0.1:8000/`, or navigate to `http://127.0.0.1:8000/docs`.

## Run Tests
To run the tests
```sh
(env)$ python manage.py test
```
