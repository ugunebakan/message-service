from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse

from user.models import User

class UserTests(APITestCase):

    def setUp(self):
        self.users_data = [
        {
            'username': 'umut.gunebakan',
            'email': 'u@z.com',
            'first_name': 'umut',
            'last_name': 'gunebakan',
        },
        {
            'username': 'adem.gunebakan',
            'email': 'a@z.com',
            'first_name': 'adem',
            'last_name': 'gunebakan',
        }
        ]

        users = [User(**user) for user in self.users_data]

        User.objects.bulk_create(users)


    def test_get_user_list(self):
        url = reverse('user-app:user-list')
        response = self.client.get(url, format='json')
        results = response.json()['results']
        # removed the id since id is not included in the data set
        [item.pop('id') for item in results]
        self.assertCountEqual(results, self.users_data)
        self.assertListEqual(results, self.users_data)

    def test_get_single_user(self):
        user_list_url = reverse('user-app:user-list')
        response = self.client.get(user_list_url, format='json')
        users = response.json()['results']
        user_id = users[0].get('id')

        user_detail_url = reverse('user-app:user-detail', args=[user_id])
        user_data = self.client.get(user_detail_url, format='json').json()
        self.assertDictEqual(users[0], user_data)

    def test_create_user(self):
        data = {
            'username': 'test.gunebakan',
            'email': 't@z.com',
            'first_name': 'test',
            'last_name': 'gunebakan',
        }

        url = reverse('user-app:user-list')
        response = self.client.post(url, data, format='json')
        user_id = response.json().get('id')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.filter(id=user_id).count(), 1)

    def test_delete_user(self):
        user_id = User.objects.first().id
        user_detail_url = reverse('user-app:user-detail', args=[user_id])
        response = self.client.delete(user_detail_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
