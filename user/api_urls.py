from rest_framework_nested import routers
from user.viewsets import UserViewSet
from message.viewsets import UserMessageViewSet
from django.conf.urls import include, url


app_name = 'user-app'

router = routers.SimpleRouter()
router.register('', UserViewSet, basename='user')

messages_router = routers.NestedSimpleRouter(
    router,
    r'',
    lookup='user'
)
messages_router.register(r'messages', UserMessageViewSet, basename='message')

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'', include(messages_router.urls)),
]
