from django.db.models import Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from message.models import Message
from message.serializers import MessageSerializer
from user.models import User
from user.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    User API.
    You can apply CRUD operations to users with
    relevent HTTP Methods.
    You can also fetch (2) new messages from
    /user/:user_id/fetchnewmessages
    Once you fetch the new messages they will be
    marked as 'read' in the DB.
    Messages for a specific user also can be fetched
    as /user/:user_id/messages,
    or a single message of a user can be fetched 
    as /user/:user_id/messages/:message_id
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['get'], detail=True)
    def fetchnewmessages(self, request, pk=None):
        filter_params = Q(from_user=pk) | Q(to_user=pk)
        queryset = Message.objects.filter(
            filter_params
        ).filter(
            is_read=False
        )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = MessageSerializer(page, many=True)
            ids = [obj.id for obj in page]
            Message.objects.filter(
                id__in=ids
            ).update(
                is_read=True
            )
            return self.get_paginated_response(serializer.data)

        serializer = MessageSerializer(page, many=True)
        queryset.update(is_read=True)
        return Response(serializer.data)
